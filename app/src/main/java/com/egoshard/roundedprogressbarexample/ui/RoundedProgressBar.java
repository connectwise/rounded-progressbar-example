package com.egoshard.roundedprogressbarexample.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ClipDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.egoshard.roundedprogressbarexample.R;


/**
 * Example class to create a rounded object similar to a
 */
public class RoundedProgressBar extends RelativeLayout {

    private ImageView fgDrawableImageView;
    private ImageView bgDrawableImageView;
    private double max = 100;
    private int level;

    /**
     * Constructor to build a rounded object form a context and set of XML attributes.
     * Inflates the object using the currect system inflater and then calls setup.
     * @param context
     * @param attrs
     */
    public RoundedProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.rounded_progress, this);
        setup(context, attrs);
    }

    /**
     * Set max increment size as an integer, generally this would be 100 if looking for a percentage complete.
     * @param max
     */
    public void setMax(int max) {
        Integer maxInt = max;
        maxInt.doubleValue();
        this.max = max;
    }

    /**
     * Main method to construct the visible object.
     * @param context
     * @param attrs
     */
    protected void setup(Context context, AttributeSet attrs) {

        // Initialize temporary array
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.com_sample_roundprogress_ui_RoundedProgressBar);

        final String xmlns = "http://schemas.android.com/apk/res-auto";
        // Create a drawable view for the foreground
        int fgResource = attrs.getAttributeResourceValue(xmlns, "fgDrawable", 0);
        fgDrawableImageView = (ImageView) findViewById(R.id.fg_image_view);
        fgDrawableImageView.setBackgroundResource(fgResource);

        // Create a drawable view for the background
        int bgResource = attrs.getAttributeResourceValue(xmlns, "bgDrawable", 0);
        bgDrawableImageView = (ImageView) findViewById(R.id.bg_image_view);
        bgDrawableImageView.setBackgroundResource(bgResource);

        // Set initial progress
        int progress = attrs.getAttributeIntValue(xmlns, "progress", 0);
        setProgress(progress);
        // Set maximum
        int max = attrs.getAttributeIntValue(xmlns, "max", 100);
        setMax(max);

        // Cause GC on temp array
        a.recycle();

        // Draw border, ideally this would be more configurable when built as a reusable component.
        // Color and weight should be passed XML context attributes.
        RoundedProgressBarBorder outline = new RoundedProgressBarBorder(context);
        addView(outline);
    }

    /**
     *
     * @param value
     */
    public void setProgress(Integer value) {
        setProgress((double) value);
    }

    /**
     * Method to clip the background image with the foreground image.
     * @param value
     */
    public void setProgress(double value) {
        ClipDrawable drawable = (ClipDrawable) fgDrawableImageView.getBackground();
        double percent = (double) value / (double) max;
        level = (int) Math.floor(percent * 10000);

        try {
            drawable.setLevel(level);
        } catch (Exception ex) {
            Log.e("com.sample.roundprogress.ui.RoundedProgressBar", ex.toString());
        }

    }

    /**
     * Inner class for the object border. Simply draws a border on the existing canvas.
     * Color and weight values are hardcoded but should be configuratble if used as a component.
     */
    private class RoundedProgressBarBorder extends View {

        private Paint paint;

        public RoundedProgressBarBorder(Context context) {
            super(context);
            paint = new Paint();
        }

        @Override
        public void onDraw(Canvas canvas) {
            paint.setColor(Color.WHITE);
            paint.setStrokeWidth(2);
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.STROKE);
            paint.setARGB(255, 100, 100, 100);
            RectF r = new RectF(1, 1, getWidth() - 2, getHeight() - 2);
            canvas.drawRoundRect(r, getHeight() / 2, getHeight() / 2, paint);
        }

    }

}
